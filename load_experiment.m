function exp_data = load_experiment(dir, N)
% exp_data = load_experiment(dir)
%
% Description: Loads data from experiment
% Inputs:
%  dir: directory containt experimental csvs
%  N:   Number or robots in swarm
% Outputs:
%  exp_data: strunct containing experimental data
%   a:     abstract state or the swarm
%   a_d:   desired abstract state of the swarm
%   ad_d:  derivative of the desired abstract state
%   sums:  sums calculated by global observer
%   x_p:   state of the parent system
%	x_p_d: desired state of the parent system
%   x_s:   array of states of the swarm
%   u_d:   array of desired inputs to the swarm
%   u:     array of actual inputs to the swarm
%

	exp_data.a     = dlmread(sprintf('%s/abs_state.csv', dir));
	exp_data.a_d   = dlmread(sprintf('%s/abs_state_des.csv', dir));
	exp_data.ad_d  = dlmread(sprintf('%s/abs_state_des_dot.csv', dir));
	exp_data.sums  = dlmread(sprintf('%s/global_obs.csv', dir));
	exp_data.x_p   = dlmread(sprintf('%s/parent_state.csv', dir));
	exp_data.x_p_d = dlmread(sprintf('%s/parent_state_des.csv', dir));
	
	for i = 1:N
		exp_data.x_s{i} = dlmread(sprintf('%s/rob%d_state.csv', dir, i-1));
		exp_data.u_d{i} = dlmread(sprintf('%s/rob%d_input.csv', dir, i-1));
		exp_data.u{i}   = dlmread(sprintf('%s/rob%d_vel_act.csv', dir, i-1));
	end
end

