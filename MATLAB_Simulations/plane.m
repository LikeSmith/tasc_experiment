clear all
close all

A_act = 1;
B_act = 1;
C_act = 1;
D_act = 1;

del = 0.5;
noise_var = eye(3)*0.001;

[X, Y] = meshgrid(-1:del:1, -1:del:1);

Z = zeros(size(X));
pts = zeros(size(X, 1)*size(X, 2), 3);

for i = 1:size(X, 1);
	for j = 1:size(X, 2);
		Z(i, j) = (D_act - A_act*X(i, j) - B_act*Y(i, j))/C_act;
		
		noise = mvnrnd(zeros(3, 1), noise_var);
		
		pts((i-1)*size(X, 1) + j, 1) = X(i, j) + noise(1);
		pts((i-1)*size(X, 1) + j, 2) = Y(i, j) + noise(2);
		pts((i-1)*size(X, 1) + j, 3) = Z(i, j) + noise(3);
	end
end

M = [pts, ones(size(pts, 1), 1)];

[U, S, V] = svd(M);

A_est = V(1, end);
B_est = V(2, end);
C_est = V(3, end);
D_est = -V(4, end);

[corners_X, corners_Y] = meshgrid([-1, 1], [-1, 1]);
corners_Z = zeros([size(corners_X), 4]);

n_act = [A_act, B_act, C_act]'/sqrt(A_act^2 + B_act^2 + C_act^2);
p_act = [0, 0, D_act/C_act]';

n_est = [A_est, B_est, C_est]'/sqrt(A_est^2 + B_est^2 + C_est^2);
p_est = [0, 0, D_est/C_est]';

if n_act'*n_est < 0
	n_est = -n_est;
end

for i = 1:size(corners_X, 1);
	for j = 1:size(corners_X, 2);
		for k = 1:4
			corners_Z(i, j, k) = (-V(4, k) - V(1, k)*corners_X(i, j) - V(2, k)*corners_Y(i, j))/V(3, k);
		end
	end
end

figure(1);
hold on;
%surf(X, Y, Z, 'FaceColor', [0 0 0.75], 'FaceAlpha', 0.5, 'EdgeColor', 'none');
%surf(corners_X, corners_Y, corners_Z(:, :, 1), 'FaceColor', [0.75 0 0], 'FaceAlpha', 0.5, 'EdgeColor', 'none');
%surf(corners_X, corners_Y, corners_Z(:, :, 2), 'FaceColor', [0 0.75 0], 'FaceAlpha', 0.5, 'EdgeColor', 'none');
% surf(corners_X, corners_Y, corners_Z(:, :, 3), 'FaceColor', [0 0 0.75], 'FaceAlpha', 0.5, 'EdgeColor', 'none');
surf(corners_X, corners_Y, corners_Z(:, :, 4), 'FaceColor', [0 0 0], 'FaceAlpha', 0.5, 'EdgeColor', 'none');
%plot3([p_act(1), p_act(1)+n_act(1)], [p_act(2), p_act(2)+n_act(2)], [p_act(3), p_act(3)+n_act(3)]);
plot3([p_est(1), p_est(1)+n_est(1)], [p_est(2), p_est(2)+n_est(2)], [p_est(3), p_est(3)+n_est(3)], 'r');
plot3(pts(:, 1), pts(:, 2), pts(:, 3), '*');
grid on

