clear all
close all

syms p1 p2 p3 real
% syms m1 m2 m3 tau J real
m1 = 0.1;
m2 = 0.2;
m3 = 0.3;
tau = 0.2;
J = 0.25;

m = [m1 m2]';
p = [p1, p2]';

Q = diag(m) + m*m'/m3;
l = 2*tau*m/m3;
D = tau^2/m3 - J;

c = 0.5*Q^-1*l;
s = 0.5*l'*Q^-1*l - D;

[V, D] = eig(Q/s);
