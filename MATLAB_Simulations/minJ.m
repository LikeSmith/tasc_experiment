clear all
close all

% syms ej theta theta_d z real
% syms J F1 F2 K1 K2 Ks2 g4 g6 real
% 
% J_dot = ((J + F1*(K1*theta + K2*theta_d)^2 + F2 - ej)*(2*F1*K1*theta_d*(K1*theta + K2*theta_d) - Ks2*ej) - 2*F1*K2*(K1*theta + K2*theta_d)*(cos(theta)*(K1*theta + K2*theta_d) + c*theta_d))/(J + F1*(K1*theta + K2*theta_d)^2 + F2 - ej + 2*F1*K2*theta_d*(K1*theta + K2*theta_d));
% 
% E_5 = J + F1*(K1*theta + K2*theta_d)^2 + F2 - ej + 2*F1*K2*theta_d*(K1*theta + K2*theta_d);
% 
% bound = (2*F1^2*(K1 + K2)^2*(K1^2 + K2)*z^4 + (F1*(K1 + K2)^2*Ks2 + 2*F1*(K1^2 + K2))*z^3 + ((J + F2)*2*F1*(K1^2 + K2) + 2*F1*(K1 + K2 + g6)*(K1 + K2^2) + Ks2)*z^2 + ((J + F2)*Ks2 + 2*F1*(K1 + K2^2)*g4)*z)/((F1*(K1 + K2)^2 + 2*F1*(K1+K2^2))*z^2 + z + J + F2);
% 
% num = 2*F1^2*(K1 + K2)^2*(K1^2 + K2)*z^4 + (F1*(K1 + K2)^2*Ks2 + 2*F1*(K1^2 + K2))*z^3 + ((J + F2)*2*F1*(K1^2 + K2) + 2*F1*(K1 + K2 + g6)*(K1 + K2^2) + Ks2)*z^2 + ((J + F2)*Ks2 + 2*F1*(K1 + K2^2)*g4)*z;
% den = (F1*(K1 + K2)^2 + 2*F1*(K1+K2^2))*z^2 + z + J + F2;
% 
% sol = solve(den, z);

J = 0.5;
F1 = 0.0125;
F2 = 0.025;
K1 = 3.1623;
K2 = 3.2859;
Ks2 = 10;
g4 = 0.02;
g6 = 1;
z = 0:0.0001:0.1806;
theta_max = 0.2;

bound = (2*F1^2*(K1 + K2)^2*(K1^2 + K2)*z.^4 + (F1*(K1 + K2)^2*Ks2 + 2*F1*(K1^2 + K2))*z.^3 + ((J + F2)*2*F1*(K1^2 + K2) + 2*F1*(K1 + K2 + g6)*(K1 + K2^2) + Ks2)*z.^2 + ((J + F2)*Ks2 + 2*F1*(K1 + K2^2)*g4)*z)./((F1*(K1 + K2)^2 + 2*F1*(K1+K2^2))*z.^2 + z + J + F2);
max(bound)