function draw_plane(n, p0, max_xy, c, a)

[~, i] = max(n);

if i == 1
	[p_z, p_y] = meshgrid(linspace(-max_xy, max_xy, 100));
	p_x = (n'*p0 - n(2)*p_z - n(3)*p_y)/n(1);
elseif i == 2
	[p_x, p_z] = meshgrid(linspace(-max_xy, max_xy, 100));
	p_y = (n'*p0 - n(1)*p_x - n(3)*p_z)/n(2);
elseif i == 3
	[p_x, p_y] = meshgrid(linspace(-max_xy, max_xy, 100));
	p_z = (n'*p0 - n(1)*p_x - n(2)*p_y)/n(3);
end

surf(p_x, p_y, p_z, 'FaceColor', c, 'FaceAlpha', a, 'LineStyle', 'none')