function draw_ellipse(R, V, C, c)

theta = linspace(0, 2*pi, 100);
pts = V*[R(1)*sin(theta); R(2)*cos(theta)] + repmat(C, 1, 100);

plot3(pts(1, :), pts(2, :), pts(3, :), c, 'linewidth', 2)
