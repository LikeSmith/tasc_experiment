clear all
close all

% syms p2 m1 m2 m3 tau J real;

m1 = 0.1;
m2 = 0.2;
m3 = 0.3;
tau = 0.2;
J = 0.25;

A2 = -4*m1^2 - 4*m1^3/m3 - 4*m1*m2^2/m3;
B2 = -8*m1*tau - 8*m1^2*tau/m3 + 8*m1^2*m2*tau/m3^2;
C2 = 4*m1*J + 4*m1^2*J/m3 - 4*m1*tau^2/m3;

p2_up = (-B2 + sqrt(B2^2 - 4*A2*C2))/(2*A2);
p2_dn = (-B2 - sqrt(B2^2 - 4*A2*C2))/(2*A2);

p2 = linspace(p2_dn, p2_up, 101);

A = m1 + m1^2/m3;
B = 2*m1*m2*p2/m3 + 2*m1*tau/m3;
C = (m1 + m2^2/m3)*p2.^2 + 2*m3*tau*p2/m3 + tau^2/m3 - J; 

p1_a = (-B + sqrt(B.^2 - 4*A*C))/(2*A);
p1_b = (-B - sqrt(B.^2 - 4*A*C))/(2*A);
p1_a(1) = -B(1)/(2*A);
p1_b(1) = -B(1)/(2*A);
p1_a(end) = -B(end)/(2*A);
p1_b(end) = -B(end)/(2*A);
p3_a = (m1/m3)*p1_a + (m2/m3)*p2 - (1/m3)*tau;
p3_b = (m1/m3)*p1_b + (m2/m3)*p2 - (1/m3)*tau;

figure
hold on
plot3([p1_a; p1_b]', [p2; p2]', [p3_a; p3_b]');
grid on
xlabel('p1')
ylabel('p2')
zlabel('p3')
