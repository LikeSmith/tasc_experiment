%
% Mapps constraints from one space to another for Tiered Abstract Swarm
% Control (TASC)
%

close all
clear all

L = 1;
vel_max = 0.1;
N = 4;
m_max = 1.5;
m_min = 0.5;
g = 9.81;
n1 = 100000;
n2 = 100000;
n3 = 100;
J_0 = 0.01;

m = rand(1, N)*(m_max-m_min) + m_min;
%m = [0.3552 0.3532 0.6762 0.4596];
m_t = sum(m);
pb = L/2*ones(2^N, N);
vb = [L/2*ones(2^(2*N), N) vel_max*ones(2^(2*N), N)];

for i = 1:2^N
	for j = 1:N
		if mod(floor((i-1)/(2^(j-1))), 2) == 1
			pb(i, j) = -pb(i, j);
		end
	end
end

for i = 1:2^(2*N)
	for j = 1:(2*N)
		if mod(floor((i-1)/(2^(j-1))), 2) == 1
			vb(i, j) = -vb(i, j);
		end
	end
end

n_e = 2^(N-1)*nchoosek(N, 1);
p_edges = zeros(n_e, 2);
cur_edge_p = 1;

for i = 1:2^N
	for j = i+1:2^N
		if norm(pb(i, :) - pb(j, :)) == L
			p_edges(cur_edge_p, 1) = i;
			p_edges(cur_edge_p, 2) = j;
			cur_edge_p = cur_edge_p + 1;
		end
	end
end

delta = linspace(0, 1, n3);

%% analysis of a
ab = zeros(2^N, 2);
a_edges = zeros(n3, 2, n_e);

ab(:, 1) = (m*pb')'*g;
ab(:, 2) = (m*(pb.^2)')';

for i = 1:n_e
	for j = 1:n3
		a_edges(j, 1, i) = (m*(pb(p_edges(i, 1), :) + (pb(p_edges(i, 2), :) - pb(p_edges(i, 1), :))*delta(j))')*g;
		a_edges(j, 2, i) = (m*((pb(p_edges(i, 1), :) + (pb(p_edges(i, 2), :) - pb(p_edges(i, 1), :))*delta(j)).^2)');
	end
end

%p = rand([N, n1])'*L - L/2;
p = [random('Beta', 0.125, 0.125, [N, floor(n1/4)])'*L - L/2; rand([N, ceil(3*n1/4)])'*L - L/2];
a = zeros(n1, 2);

a(:, 1) = (m*p')'*g;
a(:, 2) = (m*(p.^2)')';

a_abs_const = [];
a_const = [];

h = waitbar(0, 'Sorting Abstract States...');
for i = 1:n1
	waitbar(i/n1, h);
	if ~is_abs_constrained(a(i, :)', m', L/2)
		a_abs_const = [a(i, :); a_abs_const];
	else
		a_const = [a(i, :); a_const];
	end
end

tau = min(ab(:, 1)):0.001:max(ab(:, 1));
J_min = tau.^2/(g^2*m_t);

A = [min(ab(:, 1))^2, min(ab(:, 1)), 1; max(ab(:, 1))^2, max(ab(:, 1)), 1; 0 0 1];
b = [min(ab(:, 1))^2/(g^2*m_t); max(ab(:, 1))^2/(g^2*m_t); J_0];
c = A^-1*b;
%%
c = [0.007 0 0.025]'
tau_use = -10:0.01:10;
J_use = c(1)*tau_use.^2 + c(2)*tau_use + c(3);

figure(1);
hold on
plot(a_const(:, 1), a_const(:, 2), 'b.');
plot(a_abs_const(:, 1), a_abs_const(:, 2), 'r.');
%plot(ab(:, 1), ab(:, 2), 'rx');

plot(tau, J_min, 'c', 'linewidth', 2);
plot(tau_use, J_use, 'g', 'linewidth', 2);

for i = 1:n_e
	plot(a_edges(:, 1, i), a_edges(:, 2, i), 'm', 'linewidth', 2);
end

legend('Abs Constr. Pts','Part Constr. Pts', 'Lower Bound', 'A_c curve', 'Hypercube Edges', 'location', 'southeast');
xlabel('\tau_s');
ylabel('J_s');

J_max = max(min(a_edges(:, 2, :)));

%% analysis of a_dot

% n_e_v = 2^(2*N - 1)*nchoosek(2*N, 1);
% v_edges = zeros(n_e_v, 2);
% cur_edge_v = 1;
% 
% for i = 1:2^(2*N)
% 	for j = i+1:2^(2*N)
% 		if length(find(vb(i, :) - vb(j, :))) == 1
% 			v_edges(cur_edge_v, 1) = i;
% 			v_edges(cur_edge_v, 2) = j;
% 			cur_edge_v = cur_edge_v + 1;
% 		end
% 	end
% end
% 
% adotb = zeros(2^(2*N), 2);
% adot_edges = zeros(n3, 2, n_e_v);
% 
% for i = 1:2^(2*N)
% 	pos = vb(i, 1:N);
% 	vel = vb(i, (N+1):(2*N));
% 	adotb(i, 1) = g*m*vel';
% 	adotb(i, 2) = 2*m*diag(pos)*vel';
% end
% 
% for i = 1:n_e_v
% 	for j = 1:n3
% 		pos = vb(v_edges(i, 1), 1:N) + (vb(v_edges(i, 2), 1:N) - vb(v_edges(i, 1), 1:N))*delta(j);
% 		vel = vb(v_edges(i, 1), (N+1):(2*N)) + (vb(v_edges(i, 2), (N+1):(2*N)) - vb(v_edges(i, 1), (N+1):(2*N)))*delta(j);
% 		adot_edges(j, 1, i) = g*m*vel';
% 		adot_edges(j, 2, i) = 2*m*diag(pos)*vel';
% 	end
% end
% 
% v = random('Beta', 0.5, 0.5, [2*N, n2])';
% v(:, 1:N) = v(:, 1:N)*L - L/2;
% v(:, (N+1):(2*N)) = v(:, (N+1):(2*N))*2*vel_max - vel_max;
% 
% adot = zeros(n2, 2);
% 
% for i = 1:n2
% 	pos = v(i, 1:N);
% 	vel = v(i, (N+1):(2*N));
% 	adot(i, 1) = g*m*vel';
% 	adot(i, 2) = 2*m*diag(pos)*vel';
% end
% 
% figure(2)
% hold on
% plot(adot(:, 1), adot(:, 2), 'bx');
% plot(adotb(:, 1), adotb(:, 2), 'rx');
% 
% for i = 1:n_e_v
% 	plot(adot_edges(:, 1, i), adot_edges(:, 2, i), 'm');
% end
% 
% plot(adotb(:, 1), adotb(:, 2), 'rx');
% 
% legend('Monty Carlo Results', 'Hypercube Verticies', 'Hypercube Edges', 'location', 'southeast');
% xlabel('\tau_s');
% ylabel('J_s');
