function [R, V, C, A] = plane_ellipse_intersection(n, p0, M, J)
%
% [R, V, C, A] = plane_ellipse_intersection(n, p0, M, J)
% 
% This function calculates the intersection of an N dimensional
% hyperellipse centered at the origin (given by p'Mp = J) and an N
% dimensional hyperplane (given by n'*(p - p0) = 0).  This intersection is
% an N-1 dimesional hyperellipse.
%
% Parameters:
%  n  - normal of plane
%  p0 - point on the plane
%  M  - Symmetric matrix that is the quadratic coefficient of the ellipse
%  J  - constant coefficient of the ellipse
% Returns:
%  R - semi-principle radii
%  V - matrix where each column is a semi-principle axis
%  C - center of the ellipse
%  A - Transform to plane coordinate frame

N = size(n, 1);

e = abs(n'*eye(N));
[~, c] = max(e);

A = eye(N);

for i = 1:N
	A(c, i) = -n(i)/n(c);
end

A(:, c) = [];

for i = 1:N-1
	A(:, i) = A(:, i)/norm(A(:, i));
end

M_hat = A'*M*A;
J_hat = J - p0'*M*p0;

[V_hat, D] = eig(M_hat);

C_hat = p0'*M*A*V_hat;

R = zeros(N-1, 1);
b_c_hat = zeros(N-1, 1);

sum = 0;

for i = 1:N-1
	sum = sum + C_hat(i)^2/D(i, i);
end

for i = 1:N-1
	R(i) = sqrt((J_hat + sum)/D(i, i));
	b_c_hat(i) = -C_hat(i)/D(i, i);
end

V = A*V_hat;
C = A*V_hat*b_c_hat + p0;
