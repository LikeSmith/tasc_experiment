%
% Orthogonality.m
%
% Playing with orthogonality for system...
%
% Author: Kyle Crandall
%

clear all;
close all;

syms m1 m2 m3 m4 p1 p2 p3 p4 tau J a b real

Phi = [m1 m2 m3 m4 -tau; 2*m1*p1 2*m2*p2 2*m3*p3 2*m4*p4 -J];
%Phi = [m1 m2 m3 m4 -mu];

Z = null(Phi);

x_k = [a b 1]';
x_s = [1 0 0 0 0; 0 1 0 0 0; 0 0 1 0 0; 0 0 0 1 0]*Z*x_k;

Torque = simplify([m1 m2 m3 m4]*x_s)
MOI = [m1 m2 m3 m4]

solns = solve(x_s == [p1 p2 p3 p4]', p1, p2, p3, p4);

x_s = [solns.p1(1) solns.p2(1) solns.p3(1) solns.p4(1)]';

Torque = simplify([m1 m2 m3 m4]*x_s)
MOI = [m1 m2 m3 m4]
