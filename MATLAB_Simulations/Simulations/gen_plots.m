%
% gen plots
%

clear all;
close all;

load AbstractionPaper_data;
fs = 6;
fs_l = 8;
margin = 2;
fig_w1 = 7;
fig_w2 = 7;
fig_h1 = 3;
fig_h2 = 1;
label_w = 0.1875;
label_h = 0.1875;
gap_w = 0.0625;
gap_h = 0.0625;

box_w = label_w/fig_w1;
box_h = label_h/fig_h1;
box_x_off = gap_w/fig_w1 + box_w;
box_y_off = gap_h/fig_h1 + box_h;

f = figure('units', 'inches', 'position', [7, 4, fig_w1, fig_h1]);
ax = subplot(3, 3, 1);
ax.OuterPosition = [0, 0.6825, 0.31, 0.3175];
plot(PD4_data.si.t, PD4_data.si.x_p');
xlim([0, 7]);
ylim([-0.05, 0.1]);
yl = ylabel('rad,rad/s');
set(gca, 'FontSize', fs)
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'a', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')
set(yl, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0])

ax = subplot(3, 3, 2);
ax.OuterPosition = [0.3333, 0.6825, 0.31, 0.3175];
plot(PD4_data.si.t, PD4_data.si.x_s(PD4_data.si.r_pos, :)');
xlim([0, 7]);
ylim([-0.2, 0.2]);
yl=ylabel('m');
set(gca, 'FontSize', fs)
set(yl, 'Units', 'Normalized', 'Position', [-0.075, 0.5, 0]);
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'b', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(3, 3, 3);
ax.OuterPosition = [0.6666, 0.6825, 0.31, 0.3175];
yyaxis left
plot(PD4_data.si.t, PD4_data.si.a_s(1, :) - PD4_data.si.a_d(1, :))
ylim([-0.5, 0.5])
yl1=ylabel('N*m');

yyaxis right
plot(PD4_data.si.t, PD4_data.si.a_s(2, :) - PD4_data.si.a_d(2, :))
ylim([-0.05, 0.05])
yl2=ylabel('N*m^2');

xlim([0, 7]);
set(gca, 'FontSize', fs)
set(yl1, 'Units', 'Normalized', 'Position', [-0.1, 0.5])
set(yl2, 'Units', 'Normalized', 'Position', [1.075, 0.5])
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'c', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(3, 3, 4);
ax.OuterPosition = [0, 0.3650, 0.31, 0.3175];
plot(PD4_data.di.t, PD4_data.di.x_p');
xlim([0, 7]);
ylim([-0.05, 0.1]);
yl = ylabel('rad,rad/s');
set(gca, 'FontSize', fs)
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'd', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')
set(yl, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0])

ax = subplot(3, 3, 5);
ax.OuterPosition = [0.3333, 0.3650, 0.31, 0.3175];
plot(PD4_data.di.t, PD4_data.di.x_s(PD4_data.si.r_pos, :)');
xlim([0, 7]);
ylim([-0.2, 0.2]);
yl=ylabel('m');
set(gca, 'FontSize', fs)
set(yl, 'Units', 'Normalized', 'Position', [-0.075, 0.5, 0]);
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'e', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(3, 3, 6);
ax.OuterPosition = [0.6666, 0.3650, 0.31, 0.3175];
yyaxis left
plot(PD4_data.di.t, PD4_data.di.a_s(1, :) - PD4_data.di.a_d(1, :))
ylim([-0.5, 0.5])
yl1=ylabel('N*m');

yyaxis right
plot(PD4_data.di.t, PD4_data.di.a_s(2, :) - PD4_data.di.a_d(2, :))
ylim([-0.05, 0.05])
yl2=ylabel('N*m^2');

xlim([0, 7]);
set(gca, 'FontSize', fs)
set(yl1, 'Units', 'Normalized', 'Position', [-0.1, 0.5])
set(yl2, 'Units', 'Normalized', 'Position', [1.075, 0.5])
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'f', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(3, 3, 7);
ax.OuterPosition = [0, 0, 0.31, 0.3650];
plot(PD4_data.nh.t, PD4_data.nh.x_p');
xlim([0, 7]);
ylim([-0.05, 0.1]);
xlabel('time (s)');
yl = ylabel('rad,rad/s');
set(gca, 'FontSize', fs)
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'g', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')
set(yl, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0])

ax = subplot(3, 3, 8);
ax.OuterPosition = [0.3333, 0, 0.31, 0.3650];
plot(PD4_data.nh.t, PD4_data.nh.x_s(PD4_data.si.r_pos, :)');
xlim([0, 7]);
ylim([-0.2, 0.2]);
xlabel('time (s)');
yl=ylabel('m');
set(gca, 'FontSize', fs)
set(yl, 'Units', 'Normalized', 'Position', [-0.075, 0.5, 0]);
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'h', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(3, 3, 9);
ax.OuterPosition = [0.6666, 0, 0.31, 0.3650];
yyaxis left
plot(PD4_data.nh.t, PD4_data.nh.a_s(1, :) - PD4_data.nh.a_d(1, :))
ylim([-0.5, 0.5])
yl1=ylabel('N*m');

yyaxis right
plot(PD4_data.nh.t, PD4_data.nh.a_s(2, :) - PD4_data.nh.a_d(2, :))
ylim([-0.05, 0.05])
yl2=ylabel('N*m^2');

xlabel('time (s)');
xlim([0, 7]);
set(gca, 'FontSize', fs)
set(yl1, 'Units', 'Normalized', 'Position', [-0.1, 0.5])
set(yl2, 'Units', 'Normalized', 'Position', [1.075, 0.5])
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'i', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

f.Renderer = 'Painters';
saveas(gcf, 'figs/PD_Results', 'epsc')
saveas(gcf, 'figs/PD_Results.fig')

f = figure('units', 'inches', 'position', [7, 4, fig_w1, fig_h1]);
ax = subplot(3, 3, 1);
ax.OuterPosition = [0, 0.6825, 0.31, 0.3175];
plot(ARISE4_data.si.t, ARISE4_data.si.x_p');
xlim([0, 100]);
ylim([-0.1, 0.1]);
yl = ylabel('rad,rad/s');
set(gca, 'FontSize', fs)
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'a', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')
set(yl, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0])

ax = subplot(3, 3, 2);
ax.OuterPosition = [0.3333, 0.6825, 0.31, 0.3175];
plot(ARISE4_data.si.t, ARISE4_data.si.x_s(ARISE4_data.si.r_pos, :)');
xlim([0, 100]);
ylim([-0.2, 0.2]);
yl=ylabel('m');
set(gca, 'FontSize', fs)
set(yl, 'Units', 'Normalized', 'Position', [-0.075, 0.5, 0]);
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'b', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(3, 3, 3);
ax.OuterPosition = [0.6666, 0.6825, 0.31, 0.3175];
yyaxis left
plot(ARISE4_data.si.t, ARISE4_data.si.a_s(1, :) - ARISE4_data.si.a_d(1, :))
ylim([-0.015, 0.015])
yl1=ylabel('N*m');

yyaxis right
plot(ARISE4_data.si.t, (ARISE4_data.si.a_s(2, :) - ARISE4_data.si.a_d(2, :))*1000)
ylim([-2, 2])
yl2=ylabel('N*m^2*10^3');

xlim([0, 100]);
set(gca, 'FontSize', fs)
set(yl1, 'Units', 'Normalized', 'Position', [-0.1, 0.5])
set(yl2, 'Units', 'Normalized', 'Position', [1.075, 0.5])
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'c', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(3, 3, 4);
ax.OuterPosition = [0, 0.3650, 0.31, 0.3175];
plot(ARISE4_data.di.t, ARISE4_data.di.x_p');
xlim([0, 100]);
ylim([-0.1, 0.1]);
yl = ylabel('rad,rad/s');
set(gca, 'FontSize', fs)
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'd', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')
set(yl, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0])

ax = subplot(3, 3, 5);
ax.OuterPosition = [0.3333, 0.3650, 0.31, 0.3175];
plot(ARISE4_data.di.t, ARISE4_data.di.x_s(ARISE4_data.si.r_pos, :)');
xlim([0, 100]);
ylim([-0.2, 0.2]);
yl=ylabel('m');
set(gca, 'FontSize', fs)
set(yl, 'Units', 'Normalized', 'Position', [-0.075, 0.5, 0]);
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'e', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(3, 3, 6);
ax.OuterPosition = [0.6666, 0.3650, 0.31, 0.3175];
yyaxis left
plot(ARISE4_data.di.t, ARISE4_data.di.a_s(1, :) - ARISE4_data.di.a_d(1, :))
ylim([-0.015, 0.015])
yl1=ylabel('N*m');

yyaxis right
plot(ARISE4_data.di.t, (ARISE4_data.di.a_s(2, :) - ARISE4_data.di.a_d(2, :))*1000)
ylim([-2, 2])
yl2=ylabel('N*m^2*10^3');

xlim([0, 100]);
set(gca, 'FontSize', fs)
set(yl1, 'Units', 'Normalized', 'Position', [-0.1, 0.5])
set(yl2, 'Units', 'Normalized', 'Position', [1.075, 0.5])
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'f', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(3, 3, 7);
ax.OuterPosition = [0, 0, 0.31, 0.3650];
plot(ARISE4_data.nh.t, ARISE4_data.nh.x_p');
xlim([0, 100]);
ylim([-0.1, 0.1]);
xlabel('time (s)');
yl = ylabel('rad,rad/s');
set(gca, 'FontSize', fs)
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'g', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')
set(yl, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0])

ax = subplot(3, 3, 8);
ax.OuterPosition = [0.3333, 0, 0.31, 0.3650];
plot(ARISE4_data.nh.t, ARISE4_data.nh.x_s(ARISE4_data.si.r_pos, :)');
xlim([0, 100]);
ylim([-0.2, 0.2]);
xlabel('time (s)');
yl=ylabel('m');
set(gca, 'FontSize', fs)
set(yl, 'Units', 'Normalized', 'Position', [-0.075, 0.5, 0]);
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'h', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(3, 3, 9);
ax.OuterPosition = [0.6666, 0, 0.31, 0.3650];
yyaxis left
plot(ARISE4_data.nh.t, ARISE4_data.nh.a_s(1, :) - ARISE4_data.nh.a_d(1, :))
ylim([-0.015, 0.015])
yl1=ylabel('N*m');

yyaxis right
plot(ARISE4_data.nh.t, (ARISE4_data.nh.a_s(2, :) - ARISE4_data.nh.a_d(2, :))*1000)
ylim([-2, 2])
yl2=ylabel('N*m^2*10^3');

xlabel('time (s)');
xlim([0, 100]);
set(gca, 'FontSize', fs)
set(yl1, 'Units', 'Normalized', 'Position', [-0.1, 0.5])
set(yl2, 'Units', 'Normalized', 'Position', [1.075, 0.5])
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'i', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

f.Renderer = 'Painters';
saveas(gcf, 'figs/ARISE_Results', 'epsc')
saveas(gcf, 'figs/ARISE_Results.fig')

box_w = label_w/fig_w2;
box_h = label_h/fig_h2;
box_x_off = gap_w/fig_w2 + box_w;
box_y_off = gap_h/fig_h2 + box_h;

f = figure('Units', 'Inches', 'Position', [7, 4, fig_w2, fig_h2]);

ax = subplot(1, 4, 1);
ax.OuterPosition = [0, 0, 0.25, 1];
plot(ARISE20_data.nh.t, ARISE20_data.nh.x_p');
xlim([0, 100]);
ylim([-0.1, 0.1]);
yl = ylabel('rad,rad/s');
set(yl, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0])
xlabel('time (s)')
set(gca, 'FontSize', fs)
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'a', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(1, 4, 2);
ax.OuterPosition = [0.25, 0, 0.25, 1];
plot(ARISE20_data.nh.t, ARISE20_data.nh.x_s(ARISE20_data.nh.r_pos, :)');
xlim([0, 100]);
ylim([-0.2, 0.2]);
yl=ylabel('m');
xlabel('time (s)')
set(gca, 'FontSize', fs)
set(yl, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0]);
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'b', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(1, 4, 3);
ax.OuterPosition = [0.5, 0, 0.25, 1];
plot(ARISE200_data.nh.t, ARISE200_data.nh.x_p');
xlim([0, 100]);
ylim([-0.1, 0.1]);
yl = ylabel('rad,rad/s');
set(yl, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0])
xlabel('time (s)')
set(gca, 'FontSize', fs)
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'c', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

ax = subplot(1, 4, 4);
ax.OuterPosition = [0.75, 0, 0.25, 1];
plot(ARISE200_data.nh.t, ARISE200_data.nh.x_s(ARISE200_data.nh.r_pos, :)');
xlim([0, 100]);
ylim([-0.2, 0.2]);
yl=ylabel('m');
xlabel('time (s)')
set(gca, 'FontSize', fs)
set(yl, 'Units', 'Normalized', 'Position', [-0.1, 0.5, 0]);
pos = ax.Position;
box_x = pos(1) + pos(3) - box_x_off;
box_y = pos(2) + pos(4) - box_y_off;
annotation('textbox', [box_x, box_y, box_w, box_h], 'String', 'd', 'Units', 'Normalized', 'BackgroundColor', 'w', 'EdgeColor', 'k', 'FontSize', fs_l, 'Margin', margin, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'FitBoxToText', 'off')

f.Renderer = 'Painters';
saveas(gcf, 'figs/ARISE_Results_large', 'epsc')
saveas(gcf, 'figs/ARISE_Results_large.fig')
