function x_d = full_MFRMFP_SI(x, t, parent, swarm, params)
	x_p = x(1:8);
	x_s = x(9:end);
	
	tau_s = params.m'*x_s;
	J_s = params.m'*(x_s.^2);
	
	[a_d, x_p_d] = parent(x_p, t, params);
	u_s = swarm(x_s, a_d, a_d_dot, tau_s, J_s, t, params);
	
	J_s_dot = 2*params.m'*(x_s.*u_s);
	
	x_d = zeros(size(x));
end