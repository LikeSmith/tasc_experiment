function [a, x_d] = parent_ARISE(x, t, params)
	x(1) = theta;
	x(2) = theta_d;
	x(3:6) = lambda;
	x(7) = mu_1;
	x(8) = mu_2;
	
	e_1 = params.thetad(t) - theta;
	e_2 = params.thetad_d(t) - theta_d + params.alpha_1*e_1;
	
	tau_sd = -sec(theta)*(params.Yd(t)*lambda + (params.ks + 1)*e_2 - (params.ks + 1)*params.e_2_0 + mu_1);
	mu_1d = (params.ks + 1)*params.alpha_2*e_2 + params.beta*params.sgn(e_2);
	lambdad = params.lambda_0 + params.Gamma*(params.Yd_d(t)*e_2 - params.Yd_d(params.t_0)*params.e_2_0) - params.Gamma*mu_2;
	mu_2d = params.Yd_dd(t)'*e_2 - params.alpha_2*params.Yd_d(t)*e_2;
	J_sd = params.C_1*tau_sd^2 + params.C_2;
	
	a = [tau_sd; J_sd];
	x_d = [lambdad; mu_1d; mu_2d];
end 