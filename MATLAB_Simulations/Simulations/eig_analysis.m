%
% eigen analysis
%

clear all
close all

syms kd1 kd2 kp1 kp2 A1 A2 A3 A4 s

A = [A1 A2; A3 A4];
Kd = diag([kd1 kd2]);
Kp = diag([kp1 kp2]);

M = [-s*eye(2) -Kp; eye(2) -Kd-A-s*eye(2)];

p = det(M);

c = coeffs(p, s);

for i = 1:length(c)
	pretty(c(i))
end

routh_table = sym(zeros(5, 3));

routh_table(1, 1) = c(5);
routh_table(2, 1) = c(4);
routh_table(1, 2) = c(3);
routh_table(2, 2) = c(2);
routh_table(1, 3) = c(1);

routh_table(3, 1) = -det(routh_table(1:2, [1 2]))/routh_table(2, 1);
routh_table(3, 2) = -det(routh_table(1:2, [1 3]))/routh_table(2, 1);
routh_table(4, 1) = -det(routh_table(2:3, [1 2]))/routh_table(3, 1);
routh_table(5, 1) = -det(routh_table(3:4, [1 2]))/routh_table(4, 1);
