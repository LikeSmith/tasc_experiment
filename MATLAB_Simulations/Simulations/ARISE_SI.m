%
% Abstractopm Sim
%

clear all
close all

% set up experimental parameters
N = 4; % number or robots
m_max = 0.75; % maximum mass of robot
m_min = 0.25; % minimum mass of robot
L = 1; % length of beam
t_f = 100; % number of seconds to run simulation
del_t = 0.01; % simulation time step
J = 0.5; % moment of ineritia of beam
g = 9.81; % gravity acceleration v
c = 1; % damping in beam
x_p0 = [0.1; 0]; % beam initial condition
A_d = 0.07; % amplitude of desired trajectory (rad)
f_d = 0.03; % frequency of desired trajectory (Hz)
off_d = 0; % offset of desired trajectory (rad)
K = [10 0; 0 10]; % gain for abstract controller
ks = 0.1; % gain for ARISE controller
alpha_1 = 1; % filter value for e2
alpha_2 = 2; % filter value for r
beta = 0.5; % RISE gain
Gamma = diag([10, 1, 1, 10]); % adaptive gain
% estimatets for friction model used in RISE controller
gamma2 = 1000;
gamma3 = 700;
gamma5 = 1000; 
% coefficients for J trajectory
C_1 = 0.0125;
C_2 = 0.01;
tau_dist = @(t)zeros(size(t));
%tau_dist = @(t)1*0.5*(sign(t - 15) + 1);
%tau_dist = @(t)1*sin(0.5*t);
sgn = @(x)tanh(10*x);
%sgn = @(x)sign(x);

% set up arrays for experiment results
t = 0:del_t:t_f;
n = length(t);
x_p = zeros(2, n);
x_s = zeros(N, n);
a_s = zeros(2, n);
a_d = zeros(2, n);
a_d_dot = zeros(2, n);
u_s = zeros(2, n);
err = zeros(3, n);
lambda = zeros(4, n);
mu = zeros(1, n);

% set up swarm
m = zeros(N, 1);
m_t = 0;

for i = 1:N
	x_s(i, 1) = 0.8*L*rand(1) - 0.8*L/2;
	m(i, 1) = m_min + (m_max - m_min)*rand(1);
	m_t = m_t + m(i, 1);
	a_s(1, 1) = a_s(1, 1) + g*m(i, 1)*x_s(i, 1);
	a_s(2, 1) = a_s(2, 1) + m(i, 1)*x_s(i, 1)^2;
end

% set up desired trajectory and derivatives
thetad = @(t)A_d*sin(2*pi*f_d*t) + off_d;
thetad_d = @(t)A_d*2*pi*f_d*cos(2*pi*f_d*t);
thetad_dd = @(t)-A_d*(2*pi*f_d)^2*sin(2*pi*f_d*t);
thetad_ddd = @(t)-A_d*(2*pi*f_d)^3*cos(2*pi*f_d*t);

% set up controller
Yd = @(t)...
	[thetad_dd(t) tanh(gamma2*thetad_d(t))-tanh(gamma3*thetad_d(t)) tanh(gamma5*thetad_d(t)) thetad_d(t)];
Yd_d = @(t)...
	[thetad_ddd(t) gamma2.*thetad_dd(t).*sech(gamma2.*thetad_d(t)).^2-gamma3.*thetad_dd(t).*sech(gamma3.*thetad_d(t)).^2 gamma5.*thetad_dd(t).*sech(gamma5.*thetad_d(t)).^2 thetad_dd(t)];

% set initial conditions
x_p(:, 1) = x_p0;
err(1, 1) = thetad(0) - x_p(1, 1);
err(2, 1) = thetad_d(0) - x_p(2, 1) + alpha_1*err(1, 1);
err(3, 1) = thetad_dd(0) + alpha_1*(err(2, 1) - alpha_1*err(1, 1)) + alpha_2*err(2, 1);
lambda(:, 1) = [0.6, 0.011, 0.03, 0.9]';

% set integraion parameters
params.g = g;
params.m = m; 
params.f_f = @(theta_d, p)p.gamma1*(tanh(p.gamma2*theta_d) - tanh(p.gamma3*theta_d)) + p.gamma4*tanh(p.gamma5*theta_d) + p.gamma6*theta_d;
params.gamma1 = 0.01;
%params.gamma1 = 0;
params.gamma2 = 1000;
params.gamma3 = 700;
params.gamma4 = 0.02;
%params.gamma4 = 0;
params.gamma5 = 1000;
params.gamma6 = c;
params.dist = tau_dist;
params.J = J;

% run experiment
for i = 2:n
	% calculate sums and abstract state
	sum0 = 0;
	sum1 = 0;
	sum2 = 0;
	sum3 = 0;
	
	for j = 1:N
		a_s(1, i) = a_s(1, i) + g*m(j)*x_s(j, i-1);
		a_s(2, i) = a_s(2, i) + m(j)*x_s(j, i-1)^2;
		
		sum0 = sum0 + m(j)^2;
		sum1 = sum1 + m(j)^2*x_s(j, i-1);
		sum2 = sum2 + m(j)^2*x_s(j, i-1)^2;
		
		for k = j+1:N
			sum3 = sum3 + m(j)^2*m(k)^2*(x_s(j, i-1) - x_s(k, i-1))^2;
		end
	end
	
	% execute parent controller
	a_s_dot = (a_s(:, i) - a_s(:, i-1))/del_t;
	
	err(1, i) = thetad(t(i)) - x_p(1, i-1);
	err(2, i) = thetad_d(t(i)) - x_p(2, i-1) + alpha_1*err(1, i);
	err(3, i) = (err(2, i) - err(2, i-1))/del_t + alpha_2*err(2, i);
	
	tau_sd = -sec(x_p(1, i-1))*(Yd(t(i))*lambda(:, i-1) + mu(1, i-1));
	lambda_d = Gamma*Yd_d(t(i))'*err(3, i);
	mu_d = (ks + 1)*err(3, i) + beta*sgn(err(2, i));
	
	tau_sd_d = x_p(2, i-1)*tan(x_p(1, i-1))*tau_sd - sec(x_p(1, i-1))*(Yd_d(t(i))*lambda(:, i-1) + Yd(t(i))*lambda_d + mu_d);
	
	J_sd = C_1*tau_sd^2 + C_2;
	J_sd_d = 2*C_1*tau_sd*tau_sd_d;
	
	a_d(1, i) = tau_sd;
	a_d(2, i) = J_sd;
	
	a_d_dot(1, i) = tau_sd_d;
	a_d_dot(2, i) = J_sd_d;
	
	% execute swarm controller based on parent controller output
	for j = 1:N
		u_s(j, i) = (1/sum3)*[(m(j)*sum2 - m(j)*x_s(j, i-1)*sum1) (m(j)*x_s(j, i-1)*sum0 - m(j)*sum1)/2]*(K*(a_d(:, i) - a_s(:, i)) + a_d_dot(:, i));
	end
	
	% apply input to system
	f_int = @(t, x)full_sys_sim_SI(x, u_s(:, i), t, params);
	x_0 = [x_p(:, i-1); x_s(:, i-1)];
	TSPAN = [t(i-1), t(i)];
	
	[t_sim, x_sim] = ode45(f_int, TSPAN, x_0);
	
	x_p(:, i) = x_sim(end, 1:2)';
	x_s(:, i) = x_sim(end, 3:end)';
	
	% update controller states
	mu(:, i) = mu(:, i-1) + mu_d*del_t;
	lambda(:, i) = lambda(:, i-1) + lambda_d*del_t;
end

figure(1);
subplot(2, 1, 1);
plot(t, [thetad(t') thetad_d(t') x_p']);
xlabel('time (s)');
legend('\theta_d', '\omega_d', '\theta', '\omega');

subplot(2, 1, 2);
plot(t, x_s');
xlabel('time (s)');
ylabel('robot positions');

figure(2);
plot(t, [a_d' a_s']);
xlabel('time (s)');
legend('\tau_{sd}', 'J_{sd}', '\tau_s', 'J_s');

figure(3);
plot(t, err);
xlabel('time (s)');
legend('e_1', 'e_2', 'r');
