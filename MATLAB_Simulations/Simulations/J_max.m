%
%  find local minimum
%

clear all
close all

params.C1 = 0.0125;
params.C2 = 0.025;
params.K1 = -3.1623;
params.K2 = -3.2859;
params.K_s2 = 10;
params.J = 0.5;
params.f_f = @(theta_d, p)p.gamma1*(tanh(p.gamma2*theta_d) - tanh(p.gamma3*theta_d)) + p.gamma4*tanh(p.gamma5*theta_d) + p.gamma6*theta_d;
params.gamma1 = 0.01;
params.gamma2 = 1000;
params.gamma3 = 700;
params.gamma4 = 0.02;
params.gamma5 = 1000;
params.gamma6 = 1;

params.f = @(z, p)-((p.C1*(p.K1*z(1) + p.K2*z(2))*p.K1*z(2) - p.K_s2*z(3))*(p.J + p.C1*(p.K1*z(1) + p.K2*z(2))^2 + p.C2 - z(3)) - p.C1*(p.K1*z(1) + p.K2*z(2))*p.K2*(cos(z(1))*(p.K1*z(1) + p.K2*z(2)) + p.f_f(z(2), p)))/(p.C1*(p.K1*z(1) + p.K2*z(2))*p.K2*z(2) + p.J + p.C1*(p.K1*z(1) + p.K2*z(2))^2 + p.C2 - z(3));
params.n = 1000;
params.eps = 0.001;
params.del_z = 0.0001*ones(3, 1);
params.k = 0.0001;
range = [-0.2 0.2; -0.1 0.1; -0.07 0.07];

[min_v, z_min] = minsearch(params, range);

J_dot_max = -min_v;

k_d2_min = J_dot_max/cos(0.2);