%
% Abstractopm Sim
%

clear all
close all

% set up experimental parameters
N = 4; % number or robots
m_max = 0.75; % maximum mass of robot
m_min = 0.25; % minimum mass of robot
c_max = 1.5; % maximum damping coefficient for robot
c_min = .5; % minimum damping coefficient for robot
L = 1; % length of beam
t_f = 10; % number of seconds to run simulation
del_t = 0.01; % simulation time step
J = 0.5; % moment of ineritia of beam
g = 9.81; % gravity acceleration v
damp = 1; % damping in beam
x_p0 = [0.1; 0]; % beam initial condition
Kp = [35 0; 0 35]; % proportional gain for abstract controller
Kd = [1 0; 0 1]; % derivative gain for abstract controller
k_sd = 1; % additional damping to robots
% coefficients for J trajectory
C_1 = 0.0125;
C_2 = 0.01;
tau_dist = @(t)zeros(size(t));
%tau_dist = @(t)1*0.5*(sign(t - 15) + 1);
%tau_dist = @(t)1*sin(0.5*t);
% Q and R are the cost matricies for the LQR controller
Q = diag([10, 1]);
R = 1;

% set up arrays for experiment results
t = 0:del_t:t_f;
n = length(t);
x_p = zeros(2, n);
x_s = zeros(2*N, n);
a_s = zeros(2, n);
a_d = zeros(2, n);
a_d_dot = zeros(2, n);
a_d_ddot = zeros(2, n);
u_s = zeros(2, n);

% set up swarm
m = zeros(N, 1);
c = zeros(N, 1);
r_pos = 2*(1:N) - 1;
r_vel = 2*(1:N);

for i = 1:N
	x_s(r_pos(i), 1) = 0.8*L*rand(1) - 0.8*L/2;
	m(i, 1) = m_min + (m_max - m_min)*rand(1);
	c(i, 1) = c_min + (c_max - c_min)*rand(1);
	a_s(1, 1) = a_s(1, 1) + g*m(i, 1)*x_s(i, 1);
	a_s(2, 1) = a_s(2, 1) + m(i, 1)*x_s(i, 1)^2;
end
%m = [0.3552; 0.3532; 0.6762; 0.4596];
%c = [0.7290; 1.4133; 0.6524; 1.3258];
x_s(r_pos, 1) = [0.25; -0.25; 0.25; -0.25];
a_s(:, 1) = [m'*x_s(r_pos, 1); m'*x_s(r_pos, 1).^2];

% set initial conditions
x_p(:, 1) = x_p0;

% set integraion parameters
params.g = g;
params.m = m;
params.c = c;
params.f_f = @(theta_d, p)p.gamma1*(tanh(p.gamma2*theta_d) - tanh(p.gamma3*theta_d)) + p.gamma4*tanh(p.gamma5*theta_d) + p.gamma6*theta_d;
%params.gamma1 = 0.01;
params.gamma1 = 0.01;
params.gamma2 = 1000;
params.gamma3 = 700;
%params.gamma4 = 0.02;
params.gamma4 = 0.02;
params.gamma5 = 1000;
params.gamma6 = damp;
params.dist = tau_dist;
params.J = J;

% LQR Controller creation
A = [0 1; 0 damp/(J + C_2)];
B = [0; -1/(J + C_2)];
C = eye(2);
D = 0;
sys = ss(A, B, C, D);

K_p = lqr(sys, Q, R);

% run experiment
for i = 2:n
	% calculate sums and abstract state
	sum0 = 0;
	sum1 = 0;
	sum2 = 0;
	sum3 = 0;
	
	for j = 1:N
		a_s(1, i) = a_s(1, i) + g*m(j)*x_s(r_pos(j), i-1);
		a_s(2, i) = a_s(2, i) + m(j)*x_s(r_pos(j), i-1)^2;
		
		sum0 = sum0 + m(j)^2;
		sum1 = sum1 + m(j)^2*x_s(r_pos(j), i-1);
		sum2 = sum2 + m(j)^2*x_s(r_pos(j), i-1)^2;
		
		for k = j+1:N
			sum3 = sum3 + m(j)^2*m(k)^2*(x_s(r_pos(j), i-1) - x_s(r_pos(k), i-1))^2;
		end
	end
	
	% execute parent controller
	a_s_dot = (a_s(:, i) - a_s(:, i-1))/del_t;
	
	tau_sd = -K_p*x_p(:, i-1);
	tau_sd_d = -K_p*(A*x_p(:, i-1) + B*tau_sd);
	
	%tau_sd = 0.5;
	%tau_sd_d = 0;
	
	J_sd = C_1*tau_sd^2 + C_2;
	J_sd_d = 2*C_1*tau_sd*tau_sd_d;
	
	a_d(1, i) = tau_sd;
	a_d(2, i) = J_sd;
	
	a_d_dot(1, i) = tau_sd_d;
	a_d_dot(2, i) = J_sd_d;
	
	a_d_ddot(:, i) = (a_d_dot(:, i) - a_d_dot(:, i-1))/del_t;
	
	% execute swarm controller based on parent controller output
	p = x_s(r_pos, i-1);
	v = x_s(r_vel, i-1);
	
	Phi_dot = [zeros(1, N); (m.*v)'];
	da = Kp*(a_d(:, i) - a_s(:, i)) + Kd*(a_d_dot(:, i) - a_s_dot) - Phi_dot*v + a_d_ddot(:, i);
	
	for j = 1:N
		u_s(j, i) = 1/sum3*[m(j)*sum2 - m(j)*p(j)*sum1 (m(j)*p(j)*sum0 - m(j)*sum1)/2]*(m(j)*da + c(j)*a_d_dot(:, i)) - k_sd*v(j);
	end
	
	% apply input to system
	f_int = @(t, x)full_sys_sim_DI(x, u_s(:, i), t, params);
	x_0 = [x_p(:, i-1); x_s(:, i-1)];
	TSPAN = [t(i-1), t(i)];
	
	[t_sim, x_sim] = ode45(f_int, TSPAN, x_0);
	
	x_p(:, i) = x_sim(end, 1:2)';
	x_s(:, i) = x_sim(end, 3:end)';
end

figure(1);
subplot(2, 1, 1);
plot(t, x_p);
xlabel('time (s)');
legend('\theta', '\omega');

subplot(2, 1, 2);
plot(t, x_s(r_pos, :)');
xlabel('time (s)');
ylabel('robot positions');

figure(2);
[hAx, hLine1, hLine2] = plotyy(t', [a_d(1, :)' a_s(1, :)'], t', [a_d(2, :)' a_s(2, :)']);
xlabel('Time (s)');
ylabel(hAx(1), '\tau_s');
ylabel(hAx(2), 'J_s');
legend('\tau_{sd}', '\tau_s', 'J_{sd}', 'J_s');
