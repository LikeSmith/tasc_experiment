function x_d = full_sys_sim_NoHomo(x, u, t, params)
	theta = x(1);
	theta_d = x(2);
	x_s = x(3:end);
	
	tau = 0;
	J = 0;
	v_term = 0;
	x_s_dot = zeros(size(x_s));
	
	for i = 1:params.N1
		tau = tau + params.g*params.m(i)*x_s(i);
		J = J + params.m(i)*x_s(i)^2;
		v_term = v_term + params.m(i)*x_s(i)*u(i);
		x_s_dot(i) = u(i);
	end
	for i = 1:params.N2
		tau = tau + params.g*params.m(params.N1+i)*x_s(params.N1+2*i-1);
		J = J + params.m(params.N1+i)*x_s(params.N1+2*i-1)^2;
		v_term = v_term + params.m(params.N1+i)*x_s(params.N1+2*i-1)*x_s(params.N1+2*i);
		x_s_dot(params.N1+2*i-1) = x_s(params.N1+2*i);
		x_s_dot(params.N1+2*i) = (u(params.N1+i) - params.c(params.N1+i)*x_s(params.N1+2*i))/params.m(params.N1+i);
	end
	
	f_f = params.f_f(theta_d, params);
	tau_d = params.dist(t);
	
	theta_dd = -(cos(theta)*tau + 2*theta_d*v_term + f_f + tau_d)/(params.J + J);
	
	x_d = [theta_d; theta_dd; x_s_dot];
end