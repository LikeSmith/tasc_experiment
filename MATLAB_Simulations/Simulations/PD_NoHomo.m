%
% Abstractopm Sim
%

clear all
%close all

% set up experimental parameters
N1 = 5; % number of single integrator robots
N2 = 5; % number of double integrator robots 
m_max = 0.75; % maximum mass of robot
m_min = 0.25; % minimum mass of robot
c_max = 1.5; % maximum damping coefficient for robot
c_min = 0.5; % minimum damping coefficient for robot
L = 1; % length of beam
t_f = 7; % number of seconds to run simulation
del_t = 0.01; % simulation time step
J = 0.5; % moment of ineritia of beam
g = 9.81; % gravity acceleration v
damp = 1; % damping in beam
x_p0 = [0.1; 0]; % beam initial condition
Kp = [10 0; 0 10]; % proportional gain for abstract controller on double integrator
Kd = [5 0; 0 5]; % derivative gain for abstract controller on double integrator
K = [10 0; 0 10]; % gain for abstract controller on single integrator
k_sd = 1; % additional damping to robots
% coefficients for J trajectory
C_1 = 0.0125;
C_2 = 0.025;
tau_dist = @(t)zeros(size(t));
%tau_dist = @(t)1*0.5*(sign(t - 15) + 1);
%tau_dist = @(t)1*sin(0.5*t);
% Q and R are the cost matricies for the LQR controller
Q = diag([10, 1]);
R = 1;

% set up arrays for experiment results
t = 0:del_t:t_f;
n = length(t);
x_p = zeros(2, n);
x_s = zeros(N1+2*N2, n);
a_s = zeros(2, n);
a_d = zeros(2, n);
a_d_dot = zeros(2, n);
a_d_ddot = zeros(2, n);
u_s = zeros(2, n);

% set up swarm
m = zeros(N1+N2, 1);
c = zeros(N2, 1);
m_t = 0;
r_pos = [1:N1 N1+2*(1:N2)-1];
r_vel = N1 + 2*(1:N2);

for i = 1:N1
	x_s(i, 1) = 0.8*L*rand(1) - 0.8*L/2;
	m(i, 1) = m_min + (m_max - m_min)*rand(1);
	m_t = m_t + m(i, 1);
	a_s(1, 1) = a_s(1, 1) + g*m(i, 1)*x_s(i, 1);
	a_s(2, 1) = a_s(2, 1) + m(i, 1)*x_s(i, 1)^2;
end
for i = 1:N2
	x_s(r_pos(N1+i), 1) = 0.8*L*rand(1) - 0.8*L/2;
	m(N1+i, 1) = m_min + (m_max - m_min)*rand(1);
	c(N1+i, 1) = c_min + (c_max - c_min)*rand(1);
	m_t = m_t + m(i, 1);
	a_s(1, 1) = a_s(1, 1) + g*m(N1+i, 1)*x_s(r_pos(N1+i), 1);
	a_s(2, 1) = a_s(2, 1) + m(N1+i, 1)*x_s(r_pos(N1+i), 1)^2;
end

if (N1 + N2) == 4
	m = [0.3552; 0.3532; 0.6762; 0.4596];
	c = [0.7290; 1.4133; 0.6524; 1.3258];
	x_s(r_pos, 1) = [0.125; -0.125; 0.125; -0.125];
	a_s(:, 1) = [m'*x_s(r_pos, 1); m'*x_s(r_pos, 1).^2];
end

% set initial conditions
x_p(:, 1) = x_p0;

% set integraion parameters
params.g = g;
params.m = m;
params.c = c;
params.N1 = N1;
params.N2 = N2;
params.f_f = @(theta_d, p)p.gamma1*(tanh(p.gamma2*theta_d) - tanh(p.gamma3*theta_d)) + p.gamma4*tanh(p.gamma5*theta_d) + p.gamma6*theta_d;
params.gamma1 = 0.01;
%params.gamma1 = 0;
params.gamma2 = 1000;
params.gamma3 = 700;
params.gamma4 = 0.02;
%params.gamma4 = 0;
params.gamma5 = 1000;
params.gamma6 = damp;
params.dist = tau_dist;
params.J = J;

% LQR Controller creation
A = [0 1; 0 damp/(J + C_2)];
B = [0; -1/(J + C_2)];
C = eye(2);
D = 0;
sys = ss(A, B, C, D);

K_p = lqr(sys, Q, R);

% run experiment
for i = 2:n
	% calculate sums and abstract state
	sum0 = 0;
	sum1 = 0;
	sum2 = 0;
	sum3 = 0;
	sum4 = 0;
	sum5 = 0;
	
	for j = 1:N1+N2
		a_s(1, i) = a_s(1, i) + g*m(j)*x_s(r_pos(j), i-1);
		a_s(2, i) = a_s(2, i) + m(j)*x_s(r_pos(j), i-1)^2;
		
		sum0 = sum0 + m(j)^2;
		sum1 = sum1 + m(j)^2*x_s(r_pos(j), i-1);
		sum2 = sum2 + m(j)^2*x_s(r_pos(j), i-1)^2;
		
		for k = j+1:N1+N2
			sum3 = sum3 + m(j)^2*m(k)^2*(x_s(r_pos(j), i-1) - x_s(r_pos(k), i-1))^2;
		end
	end
	
	a_s_dot = (a_s(:, i) - a_s(:, i-1))/del_t;
	
	% execute parent controller
	
	tau_sd = -K_p*x_p(:, i-1);
	%tau_sd_d = -K_p*(A*x_p(:, i-1) + B*a_s(1, i));
	tau_sd_d = -K_p*[x_p(2, i-1); (-cos(x_p(1, i-1))*a_s(1, i) - x_p(2, i-1)*a_s_dot(2) - params.f_f(x_p(2, i-1), params))/(J + a_s(2, i))];
	
	J_sd = C_1*tau_sd^2 + C_2;
	J_sd_d = 2*C_1*tau_sd*tau_sd_d;
	
	a_d(1, i) = tau_sd;
	a_d(2, i) = J_sd;
	
	a_d_dot(1, i) = tau_sd_d;
	a_d_dot(2, i) = J_sd_d;
	
	a_d_ddot(:, i) = (a_d_dot(:, i) - a_d_dot(:, i-1))/del_t;
	
	% execute swarm controller based on parent controller output
	p = x_s(r_pos, i-1);
	da1 = K*(a_d(:, i) - a_s(:, i)) + a_d_dot(:, i);
	
	for j = 1:N1
		u_s(j, i) = 1/sum3*[m(j)*sum2 - m(j)*p(j)*sum1 (m(j)*p(j)*sum0 - m(j)*sum1)/2]*da1;
	end
	
	C_a = zeros(2, 2);
	for j = N1 + 1:N1+N2
		C_a(1, 1) = C_a(1, 1) + m(j)*(k_sd + c(j))*(sum2 - p(j)*sum1);
		C_a(2, 1) = C_a(2, 1) + m(j)*(k_sd + c(j))*(sum2 - p(j)*sum1)*p(j)*2;
		C_a(1, 2) = C_a(1, 2) + m(j)*(k_sd + c(j))*(sum0*p(j) - sum1)/2;
		C_a(2, 2) = C_a(2, 2) + m(j)*(k_sd + c(j))*(sum0*p(j) - sum1)*p(j);
	end
	
	C_a = C_a/sum3;
	v = x_s(r_vel, i-1);
	
	Phi_dot = [zeros(1, N2); (m(N1+1:end).*v)'];
	da2 = Kp*(a_d(:, i) - a_s(:, i)) + (Kd - C_a)*(a_d_dot(:, i) - a_s_dot) + a_d_ddot(:, i) - Phi_dot*v;
	
	for j = N1+1:N1+N2
		u_s(j, i) = 1/sum3*[m(j)*sum2 - m(j)*p(j)*sum1 (m(j)*p(j)*sum0 - m(j)*sum1)/2]*(m(j)*da2 + (c(j) + k_sd)*a_d_dot(:, i)) - k_sd*v(j-N1);
	end
	
	% apply input to system
	f_int = @(t, x)full_sys_sim_NoHomo(x, u_s(:, i), t, params);
	x_0 = [x_p(:, i-1); x_s(:, i-1)];
	TSPAN = [t(i-1), t(i)];
	
	[t_sim, x_sim] = ode45(f_int, TSPAN, x_0);
	
	x_p(:, i) = x_sim(end, 1:2)';
	x_s(:, i) = x_sim(end, 3:end)';
end

figure(1);
subplot(2, 1, 1);
plot(t, x_p');
xlabel('Time (s)');
ylabel('Parent States');
legend('\theta', '\omega');

subplot(2, 1, 2);
plot(t, x_s(r_pos, :)');
xlabel('Time (s)');
ylabel('Robot Positions');

figure(2);
% [hAx, hLine1, hLine2] = plotyy(t', a_s(1, :)' - a_d(1, :)', t', a_s(2, :)' - a_d(2, :)');
% ylim2 = get(hAx(1), 'ylim');
% ratio = ylim2(1)/ylim2(2);
% ylim1 = get(hAx(2), 'ylim');
% set(hAx(2), 'ylim', [ylim1(2)*ratio, ylim1(2)]);
% xlabel('Time (s)');
% ylabel(hAx(1), 'Error in \tau_s');
% ylabel(hAx(2), 'Error in J_s', 'rot', -90, 'pos', [11.25, -0.015]);
% legend('e_\tau', 'e_J');
yyaxis left
plot(t, a_s(1, :) - a_d(1, :))
ylabel('Error in \tau_s');
ylim_l = ylim;
ratio = ylim_l(1)/ylim_l(2);

yyaxis right
plot(t, a_s(2, :) - a_d(2, :))
ylabel('Error in J_s', 'rot', -90, 'units', 'normalized', 'pos', [1.13, 0.5]);
ylim_r = ylim;
ylim([ylim_r(2)*ratio, ylim_r(2)]);

xlabel('Time (s)');
legend('e_\tau', 'e_J');