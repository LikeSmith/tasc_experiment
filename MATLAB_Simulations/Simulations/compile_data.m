%
% compile data into structs
%

clear all;
close all;

load PD4_DI;
PD4_data.di.a_d = a_d;
PD4_data.di.a_s = a_s;
PD4_data.di.t = t;
PD4_data.di.u_s = u_s;
PD4_data.di.x_s = x_s;
PD4_data.di.x_p = x_p;
PD4_data.di.r_pos = r_pos;
PD4_data.di.r_vel = r_vel;

load PD4_SI;
PD4_data.si.a_d = a_d;
PD4_data.si.a_s = a_s;
PD4_data.si.t = t;
PD4_data.si.u_s = u_s;
PD4_data.si.x_s = x_s;
PD4_data.si.x_p = x_p;
PD4_data.si.r_pos = r_pos;
PD4_data.si.r_vel = r_vel;

load PD4_NH;
PD4_data.nh.a_d = a_d;
PD4_data.nh.a_s = a_s;
PD4_data.nh.t = t;
PD4_data.nh.u_s = u_s;
PD4_data.nh.x_s = x_s;
PD4_data.nh.x_p = x_p;
PD4_data.nh.r_pos = r_pos;
PD4_data.nh.r_vel = r_vel;

load ARISE4_DI;
ARISE4_data.di.a_d = a_d;
ARISE4_data.di.a_s = a_s;
ARISE4_data.di.t = t;
ARISE4_data.di.u_s = u_s;
ARISE4_data.di.x_s = x_s;
ARISE4_data.di.x_p = x_p;
ARISE4_data.di.r_pos = r_pos;
ARISE4_data.di.r_vel = r_vel;

load ARISE4_SI;
ARISE4_data.si.a_d = a_d;
ARISE4_data.si.a_s = a_s;
ARISE4_data.si.t = t;
ARISE4_data.si.u_s = u_s;
ARISE4_data.si.x_s = x_s;
ARISE4_data.si.x_p = x_p;
ARISE4_data.si.r_pos = r_pos;
ARISE4_data.si.r_vel = r_vel;

load ARISE4_NH;
ARISE4_data.nh.a_d = a_d;
ARISE4_data.nh.a_s = a_s;
ARISE4_data.nh.t = t;
ARISE4_data.nh.u_s = u_s;
ARISE4_data.nh.x_s = x_s;
ARISE4_data.nh.x_p = x_p;
ARISE4_data.nh.r_pos = r_pos;
ARISE4_data.nh.r_vel = r_vel;

load ARISE20_NH;
ARISE20_data.nh.a_d = a_d;
ARISE20_data.nh.a_s = a_s;
ARISE20_data.nh.t = t;
ARISE20_data.nh.u_s = u_s;
ARISE20_data.nh.x_s = x_s;
ARISE20_data.nh.x_p = x_p;
ARISE20_data.nh.r_pos = r_pos;
ARISE20_data.nh.r_vel = r_vel;

load ARISE200_NH;
ARISE200_data.nh.a_d = a_d;
ARISE200_data.nh.a_s = a_s;
ARISE200_data.nh.t = t;
ARISE200_data.nh.u_s = u_s;
ARISE200_data.nh.x_s = x_s;
ARISE200_data.nh.x_p = x_p;
ARISE200_data.nh.r_pos = r_pos;
ARISE200_data.nh.r_vel = r_vel;