function x_d = full_sys_sim_DI(x, u, t, params)
	theta = x(1);
	theta_d = x(2);
	x_s = x(3:end);
	
	tau = 0;
	J = 0;
	v_term = 0;
	x_s_dot = zeros(size(x_s));
	
	for i = 1:length(x_s)/2
		tau = tau + params.g*params.m(i)*x_s(2*(i-1)+1);
		J = J + params.m(i)*x_s(2*(i-1)+1)^2;
		v_term = v_term + params.m(i)*x_s(2*(i-1)+1)*x_s(2*(i-1)+2);
		x_s_dot(2*(i-1)+1) = x_s(2*(i-1)+2);
		x_s_dot(2*(i-1)+2) = (u(i) - params.c(i)*x_s(2*(i-1)+2))/params.m(i);
	end
	
	f_f = params.f_f(theta_d, params);
	tau_d = params.dist(t);
	
	theta_dd = -(cos(theta)*tau + 2*theta_d*v_term + f_f + tau_d)/(params.J + J);
	
	x_d = [theta_d; theta_dd; x_s_dot];
end