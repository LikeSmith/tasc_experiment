clear all
close all

N = 10000;
m = [0.3552 0.3532 0.6762 0.4596];
c = [0.7290 1.4133 0.6524 1.3258];
%p = -0.5 + rand([4, N]);
p = random('Beta', 0.5, 0.5, [4, N]) - 0.5;
A = zeros(2, 2, N);
for i = 1:N
	sum0 = 0;
	sum1 = 0;
	sum2 = 0;
	sum3 = 0;
	sum4 = 0;
	sum5 = 0;
	
	for j = 1:4
		sum0 = sum0 + m(j)^2;
		sum1 = sum1 + m(j)^2*p(j, i);
		sum2 = sum2 + m(j)^2*p(j, i)^2;
		
		for k = j+1:4
			sum3 = sum3 + m(j)^2*m(k)^2*(p(j, i) - p(k, i))^2;
		end
	end
	
	A_1 = sum(m.*(c + 1).*(sum2 - p(:, i)*sum1)')/sum3;
	A_2 = sum(m.*(c + 1).*(sum0*p(:, i) - sum1)')/2/sum3;
	A_3 = sum(m.*(c + 1).*p(:, i)'.*(sum2 - p(:, i)*sum1)')*2/sum3;
	A_4 = sum(m.*(c + 1).*p(:, i)'.*(sum0*p(:, i) - sum1)')/sum3;
	
	A(:, :, i) = [A_1 A_2; A_3 A_4];
end

figure(1)
subplot(2, 2, 1)
plot(squeeze(A(1, 1, :)), 'x')
subplot(2, 2, 2)
plot(squeeze(A(1, 2, :)), 'x')
subplot(2, 2, 3)
plot(squeeze(A(2, 1, :)), 'x')
subplot(2, 2, 4)
plot(squeeze(A(2, 2, :)), 'x')