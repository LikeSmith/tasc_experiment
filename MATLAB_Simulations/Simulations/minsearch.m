function [min_v, z_min] = minsearch(p, range)
%
% function [min, z] = minsearch(p, range)
%
% This function finds the local minimum over some range
%
% inputs:
%  p: struct
%   p.f(z, p): function to minimize
%   p.n: number of partacles to use
%   p.eps: convergence criteria
%   p.del_z: for calculating gradient
%   p.k: rate of convergence
%  range: a n x 2 matrix where the left column is the minimum and the right
%  column is the maximum for each dimension of the search space
% outputs:
%  min: minimum value of function
%  z_min: location of minimum

particles = zeros(size(range, 1), p.n);
mins = zeros(p.n, 1);
mins_last = inf*ones(p.n, 1);
 
for i = 1:p.n
	for j = 1:size(range, 1)
		particles(j, i) = range(j, 1) + (range(j, 2) - range(j, 1))*rand(1); 
	end
	
	mins(i) = p.f(particles(:, i), p);
	
	while abs(mins(i) - mins_last(i)) > p.eps
		z = particles(:, i);
		dz = zeros(size(range, 1), 1);
		
		for j = 1:size(range, 1)
			z_1 = z;
			z_1(j) = z(j) + p.del_z(j);
			f1 = p.f(z, p);
			f2 = p.f(z_1, p);
			
			dz(j) = (f2 - f1)/(2*p.del_z(j));
		end
		
		particles(:, i) = z - p.k*dz;
		
		mins_last(i) = mins(i);
		mins(i) = p.f(particles(:, i), p);
		%abs(mins(i) - mins_last(i))
		
		for j = 1:size(range, 1)
			if particles(j, i) < range(j, 1)
				particles(j, i) = range(j, 1);
				mins(i) = p.f(particles(:, i), p);
				mins_last(i) = mins(i);
			elseif particles(j, i) > range(j, 2)
				particles(j, i) = range(j, 2);
				mins(i) = p.f(particles(:, i), p);
				mins_last(i) = mins(i);
			end
		end
	end
	i
end

[min_v, min_i] = min(mins);
z_min = particles(:, min_i);
