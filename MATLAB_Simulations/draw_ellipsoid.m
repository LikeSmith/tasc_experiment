function draw_ellipsoid(C, R, c, a)

[e_x, e_y, e_z] = ellipsoid(C(1), C(2), C(3), R(1), R(2), R(3), 100);

surf(e_x, e_y, e_z, 'FaceColor', c, 'FaceAlpha', a, 'LineStyle', 'none');