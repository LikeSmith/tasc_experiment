%
% Mapps constraints from one space to another for Tiered Abstract Swarm
% Control (TASC)
%

close all
clear all

L = 1;
vel_max = 0.1;
N = 4;
n1 = 1000000;
p = rand([N, n1])*L - L/2;
m = [0.1, 0.1, 0.1, 0.1];

m_t = sum(m);

a = zeros(2, n1);
for i = 1:n1
	for j = 1:N
		a(1, i) = a(1, i) + m(j)*p(j, i);
		a(2, i) = a(2, i) + m(j)*p(j, i)^2;
	end
	
	%a(:, i) = a(:, i) / sum(m);
	a(1, i) = a(1, i) * 9.81;
	%a(2, i) = sqrt(a(2, i));
end

off = [0 mean(a(2, :))*0.5+0.25*max(a(2, :))]';
range = [min(a(1, :)), max(a(1, :))];
x = linspace(range(1), range(2), 100);
y_pos = ones(size(x))*off(2);

for i = 1:n1
	bucket = floor((a(1, i) - range(1))*99/(range(2) - range(1))) + 1;
	
	if a(2, i) > off(2)
		if a(2, i) > y_pos(bucket)
			y_pos(bucket) = a(2, i);
		end
	end
end

lower = zeros(1, length(x));

for i = 1:length(x)
	lower(i) = x(i)^2/(9.81^2*m_t);
end

figure(1)
hold on
plot(a(1, :), a(2, :), 'x');
plot(x, y_pos, 'ro');
plot(x, lower, 'c');

xlabel('\tau_s');
ylabel('J_s');
legend('points in C_a', 'upper bound points points', 'lower bound');

n1 = 1000;
n2 = 10000;

Phi = @(x_s)(1/sum(m))*[m; 2*m*diag(x_s)];
v = rand([N, n2])*vel_max - vel_max/2;

a_dot = zeros(2, n1*n2);
p = rand([N, n1])*L - L/2;

for i = 1:n1
	a_dot(:, (i-1)*n2+1:i*n2) = Phi(p(:, i))*v;
end

range = [min(a_dot(1, :)) max(a_dot(1, :))];
x = linspace(range(1), range(2), 100);
y_pos = zeros(size(x));
y_neg = zeros(size(x));

for i = 1:n1*n2
	bucket = floor((a_dot(1, i) - range(1))*99/(range(2) - range(1))) + 1;
	
	if a_dot(2, i) > 0
		if a_dot(2, i) > y_pos(bucket)
			y_pos(bucket) = a_dot(2, i);
		end
	else
		if a_dot(2, i) < y_neg(bucket)
			y_neg(bucket) = a_dot(2, i);
		end
	end
end

norms1 = x.^2 + y_pos.^2;
norms2 = x.^2 + y_neg.^2;
delta = sqrt(min([min(norms1) min(norms2)]));

theta = [0:0.1:2*pi, 0];
ball_x = delta*cos(theta);
ball_y = delta*sin(theta);

figure(2)
hold on
plot(a_dot(1, :), a_dot(2, :), 'x');
plot([x, x], [y_pos, y_neg], 'ro');
plot(ball_x, ball_y, 'c');

xlabel('\tau_{s dot}');
ylabel('J_{s dot}');
legend('points in C_{a dot}', 'boundary points', 'constraint ball');
