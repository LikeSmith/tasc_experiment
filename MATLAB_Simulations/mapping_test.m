%
% mapping_test.m
%

clear all
close all

m = [1, 2, 3]';
g = 9.81;
tau = 10;
J = 1;
p_max = 1.1;

n = m/norm(m);
p0 = tau/(g*(m'*m))*m;

M = diag(m);
p1 = [0, 0, 0]';

[R, V, C, A] = plane_ellipse_intersection(n, p0, M, J);

tic
is_abs_constrained([tau/g, J]', m, p_max)
toc

figure(3)
clf
hold on
draw_ellipsoid(p1, [sqrt(J/m(1)), sqrt(J/m(2)), sqrt(J/m(3))], [0.75, 0, 0], 0.5);
draw_plane(n, p0, max(p_max, max(sqrt(J./m))), [0, 0, 0.75], 0.5);
% draw_plane([1 0 0]', [ p_max 0 0]', p_max, [0, 0.75, 0], 0.25);
% draw_plane([1 0 0]', [-p_max 0 0]', p_max, [0, 0.75, 0], 0.25);
% draw_plane([0 1 0]', [0  p_max 0]', p_max, [0, 0.75, 0], 0.25);
% draw_plane([0 1 0]', [0 -p_max 0]', p_max, [0, 0.75, 0], 0.25);
% draw_plane([0 0 1]', [0 0  p_max]', p_max, [0, 0.75, 0], 0.25);
% draw_plane([0 0 1]', [0 0 -p_max]', p_max, [0, 0.75, 0], 0.25);
% draw_line([ p_max  p_max  p_max]', [-p_max  p_max  p_max]', 'k');
% draw_line([-p_max  p_max  p_max]', [-p_max -p_max  p_max]', 'k');
% draw_line([-p_max -p_max  p_max]', [ p_max -p_max  p_max]', 'k');
% draw_line([ p_max -p_max  p_max]', [ p_max  p_max  p_max]', 'k');
% draw_line([ p_max  p_max -p_max]', [-p_max  p_max -p_max]', 'k');
% draw_line([-p_max  p_max -p_max]', [-p_max -p_max -p_max]', 'k');
% draw_line([-p_max -p_max -p_max]', [ p_max -p_max -p_max]', 'k');
% draw_line([ p_max -p_max -p_max]', [ p_max  p_max -p_max]', 'k');
% draw_line([ p_max  p_max  p_max]', [ p_max  p_max -p_max]', 'k');
% draw_line([-p_max  p_max  p_max]', [-p_max  p_max -p_max]', 'k');
% draw_line([-p_max -p_max  p_max]', [-p_max -p_max -p_max]', 'k');
% draw_line([ p_max -p_max  p_max]', [ p_max -p_max -p_max]', 'k');
draw_ellipse(R, V, C, 'c');
draw_line(C, C+V(:, 1)*R(1), 'r');
draw_line(C, C+V(:, 2)*R(2), 'g');

grid on
axis equal

xlabel('p_1');
ylabel('p_2');
zlabel('p_3');
