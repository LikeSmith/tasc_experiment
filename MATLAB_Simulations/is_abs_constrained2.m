function const = is_abs_constrained2(a, m, p_max)
%
% const = is_abs_constrained2(a, m)
%
% Checks if an abstract state is absolutely constrained.  Assumps absrtact
% state as follows:
%  a(1) = m'*p
%  a(2) = p'*M*p
% where M = diag{m} and p is the state of the swarm.  Checks against
% constriant:
%  ||p||_inf <= p_max
%
% parameters:
%  a     - abstract state to test
%  m     - mass vector
%  p_max - maximum allowable position of a robot
% Returns:
%  const - boolean, true if absolutely constrained
%

N = length(m);
const = true;

% find intersection
n = m/norm(m);
p0 = a(1)/(m'*n)*n;

[R, V, C, A] = plane_ellipse_intersection(n, p0, diag(m), a(2));

