function const = is_abs_constrained(a, m, p_max)
%
% const = is_abs_constrained(a, m)
%
% Checks if an abstract state is absolutely constrained.  Assumps absrtact
% state as follows:
%  a(1) = m'*p
%  a(2) = p'*M*p
% where M = diag{m} and p is the state of the swarm.  Checks against
% constriant:
%  ||p||_inf <= p_max
%
% parameters:
%  a     - abstract state to test
%  m     - mass vector
%  p_max - maximum allowable position of a robot
% Returns:
%  const - boolean, true if absolutely constrained
%

N = length(m);
const = true;

% find intersection
n = m/norm(m);
p0 = a(1)/(m'*n)*n;

[R, V, C, A] = plane_ellipse_intersection(n, p0, diag(m), a(2));

V_hat = pinv(A)*V;

% check each boundary condition
for i = 1:N
	n = zeros(N, 1);
	p1 = zeros(N, 1);
	p2 = zeros(N, 1);
	n(i) = 1;
	p1(i) = p_max;
	p2(i) = -p_max;
% 	b_nt = V_hat'*A'*n;
% 	b_nt = V_hat'*b_nt/norm(b_nt);
% 	b_0t = V_hat'*(n'*(p1 - C) - n'*(p0 - C))*b_nt;
% 	b_1t = V_hat'*(n'*(p2 - C) - n'*(p0 - C))*b_nt;
	b_n = V'*n/norm(V'*n);
	b_0 = (n'*(p1 - C))*b_n;
	b_1 = (n'*(p2 - C))*b_n;
	
	r = plane_ellipse_intersection(b_n, b_0, diag(1./(R.^2)), 1);
	
	if isreal(r)
		const = false;
		return
	end
		
	r = plane_ellipse_intersection(b_n, b_1, diag(1./(R.^2)), 1);
	
	if isreal(r)
		const = false;
		return
	end
end
