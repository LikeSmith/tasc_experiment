clear all
close all

% setup vectors
local_dir = [1.0, 0.0, 0.0];

filt_size = 10;

% load data
plane_dat = csvread('Plane.csv');
Rob1_dat = csvread('Rob1.csv');
Rob2_dat = csvread('Rob2.csv');
Rob3_dat = csvread('Rob3.csv');
Rob4_dat = csvread('Rob4.csv');

t = plane_dat(:, 1);
plane_p = plane_dat(:, 2:4);
plane_q = plane_dat(:, 5:8);
n = size(t, 1);

rob_p = zeros(n, 3, 4);
rob_t = zeros(n, 4);

rob_p(:, :, 1) = Rob1_dat(44:end, 2:4);
rob_p(:, :, 2) = Rob2_dat(44:end, 2:4);
rob_p(:, :, 3) = Rob3_dat(44:end, 2:4);
rob_p(:, :, 4) = Rob4_dat(19:end, 2:4);

rob_t(:, 1) = Rob1_dat(44:end, 1);
rob_t(:, 2) = Rob2_dat(44:end, 1);
rob_t(:, 3) = Rob3_dat(44:end, 1);
rob_t(:, 4) = Rob4_dat(19:end, 1);


% check time
diff = 0.0;
for i = 1:3
	for j = i:4
		diff = diff + norm(rob_t(:, i) - rob_t(:, j));
	end
end

t = t - t(1);

% initialized arrays

theta = zeros(n, 1);
p = zeros(n, 4);

% calculate theta and p
for i = 1:n
	dir = quatrotate(plane_q(i, :), local_dir);
	
	for j = 1:4
		p(i, j) = dot(dir, rob_p(i, :, j) - plane_p(i, :));
	end
	
	theta(i, 1) = asin(dir(3));
end

figure();
plot(t, theta);
xlabel('time (s)')
ylabel('theta (rad)')

figure();
plot(t, p);
xlabel('time (s)');
ylabel('robot position (m)');
legend('p1', 'p2', 'p3', 'p4');
