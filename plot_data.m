%
% plot_data
%

clear all;
close all;

exp_path = 'MFRMFP4/data/exp1';
num_robs = 4;

exp_data = load_experiment(exp_path, num_robs);

figure(1)
plot(exp_data.a(:, 1), exp_data.a(:, 2:3), exp_data.a_d(:, 1), exp_data.a_d(:, 2:3));

figure(2)
plot(exp_data.x_p(:, 1), exp_data.x_p(:, 2:3), exp_data.x_p_d(:, 1), exp_data.x_p_d(:, 2:3));

